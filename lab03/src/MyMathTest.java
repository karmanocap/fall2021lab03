import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class MyMathTest {
    @Test
    public void testOnOrigin(){
        assertEquals(MyMath.getQuadrant(0,0),"On an axis");
       }

       @Test
       public void testQ1(){
        assertEquals(MyMath.getQuadrant(1,1),"I");
       }
       @Test
       public void testQ2(){
        assertEquals(MyMath.getQuadrant(-1,1),"II");
       }
       @Test
       public void testQ3(){
        assertEquals(MyMath.getQuadrant(-1,-1),"III");
       }
       @Test
       public void testQ4(){
        assertEquals(MyMath.getQuadrant(1,-1),"IV");
       }
       @Test
       public void testYAxis(){
        assertEquals(MyMath.getQuadrant(0,1),"On an axis");
        assertEquals(MyMath.getQuadrant(1,0),"On an axis");
    
       }
       @Test
       public void testXAxis(){
        assertEquals(MyMath.getQuadrant(1,0),"On an axis");
    
       }



}

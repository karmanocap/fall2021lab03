//Hamza Jaidi 2031769

package LinearAlgebra;

public class Vector3d {
    private double x;
    private double y;
    private double z;

    public Vector3d(double xInp, double yInp, double zInp) {
        this.x = xInp;
        this.y = yInp;
        this.z = zInp;
    }

    public double getX() {
        return this.x;
    }
    
    public double getY() {
        return this.y;
    }

    public double getZ() {
        return this.z;
    }

    public double magnitude() {
        return (Math.sqrt(((x*x)+(y*y)+(z*z))));
    }

    public double dotProduct(Vector3d vectors) {
        return ((this.x * vectors.x) + (this.y * vectors.y) + (this.z * vectors.z));
    }

    public Vector3d add(Vector3d vectors) {
        Vector3d newVector = new Vector3d((this.x + vectors.x), (this.y + vectors.y), (this.z + vectors.z));
        return newVector;
    }
}

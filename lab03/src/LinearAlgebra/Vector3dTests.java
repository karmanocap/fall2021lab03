//Hamza Jaidi 2031769

package LinearAlgebra;

import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class Vector3dTests {
    @Test
    public void getTest() {
        Vector3d vectors = new Vector3d(4.4, 5.5, 6.6);
        assertEquals(vectors.getX(), 4.4);
        assertEquals(vectors.getY(), 5.5);
        assertEquals(vectors.getZ(), 6.6);
    }

    @Test
    public void magnitudeTest() {
        Vector3d vectors = new Vector3d(0,4,3);
        assertEquals(vectors.magnitude(),5);
    }

    @Test
    public void dotTest() {
        Vector3d vectors = new Vector3d(1,2,3);
        Vector3d vector = new Vector3d(1,2,3);
        assertEquals(vectors.dotProduct(vector),14);
    }

    @Test
    public void addTest() {
        Vector3d vectors = new Vector3d(1,2,3);
        Vector3d vector = new Vector3d(1,2,3);
        assertEquals(vectors.add(vector).getX(),2);
        assertEquals(vectors.add(vector).getY(),4);
        assertEquals(vectors.add(vector).getZ(),6);
    }
}

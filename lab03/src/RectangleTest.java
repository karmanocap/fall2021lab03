import static org.junit.jupiter.api.Assertions.*;
import org.junit.Test;

public class RectangleTest {

    @Test
    public void constructorTest(){
        Rectangle rect = new Rectangle(2,3);
        assertEquals(rect.getLength(),2);
        assertEquals(rect.getWidth(),3);
        rect = new Rectangle(8,12);
        assertEquals(rect.getLength(),8);
        assertEquals(rect.getWidth(),12);
    }

    @Test
    public void testGetArea(){
        Rectangle rect = new Rectangle(4,5);
        assertEquals(rect.getArea(),20);
    }

}

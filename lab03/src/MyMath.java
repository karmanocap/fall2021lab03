public class MyMath {
    public static String getQuadrant(double x, double y){
        if (x < 0 && y < 0) { return "III"; }
        if (x <= 0 && y > 0) { return "II";}
        if (x > 0 && y > 0 ) { return "I"; }
        if (x > 0 && y <= 0 ) { return "IV"; }
          return "On an axis";
    }   
}
